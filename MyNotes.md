### Useful Tips 

* [Bitbucket Supported Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Publish Website on BitBucket](https://confluence.atlassian.com/display/BITBUCKET/Publishing+a+Website+on+Bitbucket) | [TechRepublic: Hosting Static Websites](http://www.techrepublic.com/article/host-static-websites-for-free-with-bitbuckets-git-feature/)
* [Portmanteau: VC with BitBucket and Dropbox](http://www.portmanteaudesigns.com/blog/2015/01/20/version-control-for-beginners-bitbucket-dropbox-coda2/)
* [Code in the Cloud with BitBucket](https://blog.bitbucket.org/2015/02/11/coding-in-the-cloud-with-bitbucket/)

### Articles About BitBucket

* [Makeuseof: 4 reasons to use it](http://www.makeuseof.com/tag/love-github-4-reasons-why-you-should-host-your-code-on-bitbucket/)
* [Lifehacker: 4 alternative to Google Code](http://lifehacker.com/the-best-alternatives-to-google-code-for-your-programmi-1691688947)

### My Notes and References

* [John Gruber Markdown Syntax](https://daringfireball.net/projects/markdown/syntax)

### Git articles/Tutorials

* [Glossary with Step Procs](https://www.atlassian.com/git/glossary)
* [Get Started with Git](http://alistapart.com/article/get-started-with-git)
* [Tutorials](https://www.atlassian.com/git/tutorials)
* [BitBucket 101](https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101)
